local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- [Basic Keymaps]
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Install your plugins here
require('lazy').setup({
-- StatusLine/Buffer
{
  'nvim-lualine/lualine.nvim',
  dependencies = { 'kyazdani42/nvim-web-devicons', lazy = true }
},
'noib3/nvim-cokeline',
-- Fuzzy Finder
'junegunn/fzf',
{
    'nvim-telescope/telescope.nvim', tag = '0.1.5',
      dependencies = { 'nvim-lua/plenary.nvim' }
},
-- Code Completion
{
  'VonHeikemen/lsp-zero.nvim',
  branch = 'v1.x',
  dependencies = {
    -- LSP Support
    {'neovim/nvim-lspconfig'},             -- Required
    {'williamboman/mason.nvim'},           -- Optional
    {'williamboman/mason-lspconfig.nvim'}, -- Optional

    -- Autocompletion
    {'hrsh7th/nvim-cmp'},         -- Required
    {'hrsh7th/cmp-nvim-lsp'},     -- Required
    {'hrsh7th/cmp-buffer'},       -- Optional
    {'hrsh7th/cmp-path'},         -- Optional
    {'saadparwaiz1/cmp_luasnip'}, -- Optional
    {'hrsh7th/cmp-nvim-lua'},     -- Optional

    -- Snippets
    {'L3MON4D3/LuaSnip'},             -- Required
    {'rafamadriz/friendly-snippets'}, -- Optional
  }
},
-- use 'Exafunction/codeium.vim'
-- use {
--     "jcdickinson/codeium.nvim",
--     requires = {
--         "nvim-lua/plenary.nvim",
--         "hrsh7th/nvim-cmp",
--     },
--     config = function()
--         require("codeium").setup({
--         })
--     end
-- }
-- Language Support

-- Themes
'Mofiqul/dracula.nvim',
'RRethy/nvim-base16',

-- File Managers
{
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  init = function()
require("nvim-tree").setup {
actions = {
 open_file = {
  quit_on_open = true,
  }
 },
}
  end,
},

-- Lua Plugins
'nvim-lua/plenary.nvim',

-- Look & Layout
'kamykn/popup-menu.nvim',
{'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'},
'kyazdani42/nvim-web-devicons',
'sanfusu/neovim-undotree',
'norcalli/nvim-colorizer.lua',
'sgtpep/pmenu',
'goolord/alpha-nvim',
({
  "folke/noice.nvim",
  dependencies = {
    -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
    "MunifTanjim/nui.nvim",
    -- OPTIONAL:
    --   `nvim-notify` is only needed, if you want to use the notification view.
    --   If not available, we use `mini` as the fallback
    "rcarriga/nvim-notify",
    }
}),
{
  'lewis6991/gitsigns.nvim',
  -- tag = 'release' -- To use the latest release (do not use this if you run Neovim nightly or dev builds!)
},
-- Other Plugins
'jiangmiao/auto-pairs',
'gelguy/wilder.nvim',
'lambdalisue/suda.vim',
'gbprod/yanky.nvim',
{
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 100
  end,
},
{
    'numToStr/Comment.nvim',
    opts = {
        -- add any options here
    },
    lazy = false,
},
{
  'phaazon/hop.nvim',
  branch = 'v2', -- optional but strongly recommended
},
'stevearc/dressing.nvim',
({
  'mrjones2014/legendary.nvim',
  -- sqlite is only needed if you want to use frecency sorting
  -- requires = 'kkharji/sqlite.lua'
}),
-- use {'tzachar/cmp-tabnine', run='./install.sh', requires = 'hrsh7th/nvim-cmp'}
{'akinsho/toggleterm.nvim', version = "*", config = true},

})
